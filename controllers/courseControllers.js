const Course = require("../models/Course");
const auth = require("../auth");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/

module.exports.addCourse = (req, res) => {
	// Create a variable and instantiates the "Course" model 
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		let newCourse = new Course({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		})

		// console.log(newCourse);
		newCourse.save()
		// Course created successfully
		.then(course => {
			console.log(course);
			res.send(true);
		})
		.catch(err => {
			console.log(err);
			res.send(false);
		})
	}	
	else {
		res.send({message: "You don't have access to this page!"})
	}
}

// Retrieve all courses
/*
	Steps:
	1. Retrieve all the courses (active/inactive) from the database
	2. Verify the role of the current user (Admin).
*/

module.exports.getAllCourses = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		return Course.find({}).then(result => {
			res.send(result);
		})
	}
	else {
		return res.status(401).send("You don't have access to this page!");
	}
}

// Retrieve All Active Courses
/*
	Step:
		1. Retrieve all the courses from the database with the property "isActive" to true.
*/
module.exports.getAllActive = (req, res) => {
	return Course.find({isActive: true}).then(result => res.send(result));
}

// Retrieving a specific coursee
/*
	Step:
	1. Retrieve the course that matches thee course ID provided from the URL
*/
module.exports.getCourse = (req,res) => {
	console.log(req.params.courseId);
	
	return Course.findById(req.params.courseId).then(result => res.send(result));
}

// Update a course

module.exports.updateCourse = (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin){
		let updateCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		}

		// Syntax
			// findByIdAndUpdate(documentID, updatesToBeApplied, {new: true})

		return Course.findByIdAndUpdate(req.params.courseId, updateCourse, {new: true})
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		});
	}
	else {
		return res.status(401).send("You don't have access to this page!");
	}
}

// Archive a course
	// Soft delete happens when a course status (isActive) is set to false.

module.exports.archiveCourse = (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	
	let updateIsActiveField = {
		isActive: req.body.isActive
	}

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(req.params.courseId, updateIsActiveField)
		.then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}
	else {
		return res.status(401).send("You don't have access to this page!");
	}
}
