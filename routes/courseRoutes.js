const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

console.log(courseControllers);

// Route for creating a course
router.post("/", auth.verify, courseControllers.addCourse);

// Route for viewing all courses (admin only)
router.get("/all", auth.verify, courseControllers.getAllCourses);

// Route for viewing a specific course (all users)
router.get("/", courseControllers.getAllActive);

// Route for viewing all active courses (all users)
router.get("/:courseId", courseControllers.getCourse);

router.put("/:courseId", auth.verify,courseControllers.updateCourse);

router.patch("/archive/:courseId", auth.verify,courseControllers.archiveCourse);

module.exports = router;